
# API Documentation
- [Creating a new user](#markdown-header-creating-a-new-user)
- [Authenticating with email and password](#markdown-header-authenticating-with-email-and-password)
- [Authenticating with Facebook Token](#markdown-header-authenticating-with-facebook-token)
- [Getting User Profile](#markdown-header-getting-user-profile)


## Creating a new user

**POST /user**

```
## request headers
Content-Type: application/json

## request body
{
	"full_name":"Fulano de Tal",
	"email":"fulano@test.me",
	"password":"mypass",
	"password_confirmation":"mypass"
}
```

```
## response body
{
	"id":7,
	"full_name":"Fulano de Tal",
	"email":"fulano@test.me",
	"account_status":"pending",
	"created_at":"2019-08-22 23:29:49",
	"updated_at":"2019-08-22 23:29:49"
}
```


## Authenticating with email and password

**POST /auth**

```
## request headers 
Content-Type: application/json

## request body
{
    "strategy":"local",
    "email":"fulano@test.me",
    "password":"asd"
}
```

```
## response headers
Content-Type: application/json

## response body
{

    "type":"bearer",
    "token":"eyJhbGciOiJIUzI.eyJ1aWQiOjcsImlhd.dZx1piFHfrzFSxI6w",
    "refreshToken":null
}
```

## Authenticating with Facebook Token

First you need a válid facebook token.

**POST /auth**

```
## request headers
Content-Type: application/json

## request body
{
    "strategy":"facebookToken",
    "access_token":"EAAGr0tOEGaQBAEnEqybbWj0....."
}
```

```
## response headers
Content-Type: application/json

## response body
{
    "type":"bearer",
    "token":"eyJhbGciOiJIUzI.eyJ1aWQiOjcsImlhd.dZx1piFHfrzFSxI6w",
    "refreshToken":null
}
```

## Getting User Profile

**GET /user/profile**

```
## request headers
Content-Type: application/json
Authorization: Bearer eyJhbGciOiJIUzI.eyJ1aWQiOjcsImlhd.dZx1piFHfrzFSxI6w
```

```
## response headers
Content-Type: application/json

## response body
{
	"id":7,
	"full_name":"Fulano de Tal",
	"email":"fulano@test.me",
	"account_status":"pending",
	"created_at":"2019-08-22 23:29:49",
	"updated_at":"2019-08-22 23:29:49"
}
```
