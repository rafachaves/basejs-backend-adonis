'use strict'

const User = use('App/Models/User');
const Persona = use('Persona');
const Env = use('Env');
const Hash = use('Hash')

const {Facebook, FacebookApiException} = use('fb');

class AuthController {
  async store({request, auth, response}){
    const strategy = request.input('strategy', 'local');
    let user;
    if(strategy == 'facebookToken' || strategy == 'facebook'){
      const fb = new Facebook({
        appId: Env.get('FACEBOOK_APP_ID'),
        appSecret: Env.get('FACEBOOK_APP_SECRET'),
        accessToken: request.input('access_token')
      });

      const fb_user_data = await fb.api('/me?fields=id,name,email');

      user = await User.findOrCreate(
        {
          email: fb_user_data.email
        },
        {
          full_name: fb_user_data.name,
          email: fb_user_data.email,
          password: "@todo: hash uníco",
          account_status: 'active'
        });

    } else {
      let payload = request.only(['strategy', 'strategy', 'email', 'password'])
      payload.uid = payload.email;
      user = await Persona.verify(payload)
    }

    return await auth.generate(user)
  }
}

module.exports = AuthController
