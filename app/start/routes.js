'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')
const Env = use('Env');
const Yaml = use('js-yaml');

Route.get('users/profile', 'UserController.profile').middleware(['auth']);

Route.resource('/users', 'UserController');
Route.resource('/auth', 'AuthController');

Route.get('config', async ({request, response}) => {
  const config = {
    facebook:{
      appId: Env.get('FACEBOOK_APP_ID'),
      scope: Env.get('FACEBOOK_SCOPE')
    }
  };

  if(request.format() == 'json'){
    response.type('application/json');
    return config;
  }

  if(request.format() == 'yaml' || request.format() == 'yml'){
    response.type('application/x-yaml');
    return Yaml.dump(config);
  }

  return `facebookAppId=${config.facebook.appId}`;


}).formats(['json','yaml', 'yml']);
