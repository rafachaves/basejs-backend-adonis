FROM node:10-jessie

RUN npm install pm2 @adonisjs/cli -g

COPY app /app

WORKDIR /app

RUN npm install

COPY entrypoint.sh /entrypoint.sh

ENTRYPOINT /entrypoint.sh

EXPOSE 6513
