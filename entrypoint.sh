#!/bin/bash

if [ "$APP_MODE" = "development" ]; then
  npm install

  adonis migration:run
  pm2-dev ./server.js

else
  adonis migration:run
  pm2-runtime ./server.js -i $PM2_PROCESSES
fi
exec "$@"
